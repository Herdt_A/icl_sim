function main_NumIntegrate(PreferencesObj,LaserObj,CavityObj)

clc
clear variables
close all hidden


tic
%% ############# VERSION history #############
% v0.0  20200513 Adonis Bogris - INITIAL
% v0.1  20200514 Andreas Herdt - Modified for a better handling
% v1.0  20200518 Andreas Herdt - Including Output of Laser wavelength based Fourier transform
% v1.01 20200521 Andreas Herdt - Output with figures
% v2.00 20201021 Andreas Herdt - Moved into a function & added



%% Load and calculate Objects

% Calculates values for rate quation solver
TimeWindow          = PreferencesObj.SamplingTime*PreferencesObj.Samples;                                                     % time window
TimeArray           = PreferencesObj.SamplingTime:PreferencesObj.SamplingTime:TimeWindow;                                     % time array
SamplingRadFreq     = 2*pi/TimeWindow;                                                          % Sampling frequency

TotalBandWidth      = 1/PreferencesObj.SamplingTime*2*pi;                                                      % total bandwidth
RadFrequencyValues  = -TotalBandWidth/2:SamplingRadFreq:TotalBandWidth/2-SamplingRadFreq;       % rad. frequency array, from -BWw/2 to BWw/2-dw, step=dw
FrequencyValues     = RadFrequencyValues/2/pi;                                                  % frequency array



% Physical constants ############ LOAD Physical constants ############
PhysConst =  Constants.PhysConst;


% calculated laser parameters
groupVelocity       = PhysConst.Lightspeed/LaerObj.refIndex;                % group velocity
tau_ph              = 1/(groupVelocity*(LaserObj.am + LaserObj.aint));      % Photon lifetime
f_opt               = c/LaserObj.wavelength;                                % optical frequency
f_optrad            = 2*pi*f_opt;                                           % rad. frequency of laser
gain_all            = LaserObj.gain*LaserObj.numcascades;                   % total gain
tr                  = 2*LaserObj.laserlength*LaserObj.refIndex/c;           % Laser cavity frequency
factor              = h*groupVelocity*f_opt*LaserObj.am/2;                  % Factor converts sqrt(num of photons) to energy
mm                  = 1:LaserObj.nmodes;                                    % matrix of modes
sep                 = 1+1*(1*(mm-LaserObj.nmodes/2)...
                       *LaserObj.DFm/LaserObj.DBW).^2;                      % mode separation

% Coupling parameters ############ LOAD CavityObject ############
kext_i_21           = CavityObj.Inj2to1;       % injection level laser
kext_i_12           = CavityObj.Inj1to2;
CoupPhs             = CavityObj.CoupPhs;

% calculated Coupling parameters
Tdelay              = CavityObj.Lcavity/PhysConst.Lightspeed;
Nd                  = floor(Tdelay/PreferencesObj.SamplingTime);


%fitting - calculate Ith, slope
Ith_f     = PhysConst.ElementaryCharge/tau_c*(1/tau_ph/gain_all+LaserObj.N_0);
slope_f   = tau_ph*groupVelocity*PhysConst.Planck*f_optrad*LaserObj.am/PhysConst.ElementaryCharge/4/pi;


%% Initialize 







    % Calculate the number of 

    len = numel(PreferencesObj.Detuning);
    waitbar = Util.parfor_progressbar(len,'Calculating');
    Ef = nan(Samples2,len);
    Ef2 = nan(Samples2,len);
    
    parfor kj=1:len
        waitbar.iterate(1);
        
        
        ActDetVal = PreferencesObj.Detuning(kj); % detuning value for this iteration
        
        Ei=nan(nmodes, PreferencesObj.Samples);
        Einj_i=nan(nmodes, PreferencesObj.Samples)*100;
        
        Ei(1,1)      = 0;
        Einj_i(1,1)  = 0;
        
        % modify random number generation
        rng('shuffle')
        E=0;
        Einj=0;
        
        % rate equation modelling
        %master
        Ib1=ICL1.current;
        Ib2=ICL1.current;
        
        
        NN1=zeros(1, PreferencesObj.Samples); %matric of carrier number per time
        NN2=zeros(1, PreferencesObj.Samples);
        
        %modeling of rate equations with mutual coupling - the model can support
        %multi-mode operation
        
        tic
        for k=1:PreferencesObj.Samples-1
            
            %      E=0;
            %      for mm=1:ms
            %          E=E+Ei(mm, k).*exp(-i*(mm-ms/2)*DFm.*t(k)*2*pi);
            %      end
            Pow1=sum(abs(Einj_i(mm, k)).^2);
            %E=sum(Ei(mm, k).*(exp(-i*(mm-ms/2)*DFm.*t(k)*2*pi))');
            Pow2=sum(abs(Ei(mm, k)).^2);
            %Pow=abs(E).^2; %%%% power is also including the FWM interaction sum(Eiexp(i*DWi*t))
            
            GG1=gain_all*(NN1(k)-N_0)./(sep.*(1+SatPam*Pow1));
            GGa1=gain_all./(sep.*(1+SatPam*Pow1));
            GGb1=GG1-GGa1*NN1(k);
            
            ae1=(1+1i*LWE)/2.0*(GG1-1/tau_ph);
            
            GG2=gain_all*(NN2(k)-N_0)./(sep.*(1+SatPam*Pow2));
            GGa2=gain_all./(sep.*(1+SatPam*Pow2));
            GGb2=GG2-GGa2*NN2(k);
            
            ae2=(1+1i*LWE)/2.0*(GG2-1/tau_ph);
            
            
            %     for m=1:ms
            %            be(m)=add_noise(sqrt(4*NN(k)*bb/dt), 0)+1i*add_noise(sqrt(4*NN(k)*bb/dt), 0);
            %       end
            
            Pow_Ga1=sum(GGa1.*(abs(Einj_i(:, k)).^2)');
            Pow_Gb1=sum(GGb1.*(abs(Einj_i(:, k)).^2)');
            an1=-1/tau_c-Pow_Ga1;
            bn1=Ib1/q-Pow_Gb1;
            
            Pow_Ga2=sum(GGa2.*(abs(Ei(:, k)).^2)');
            Pow_Gb2=sum(GGb2.*(abs(Ei(:, k)).^2)');
            an2=-1/tau_c-Pow_Ga2;
            bn2=Ib2/q-Pow_Gb2;
            
            
            if (k<Nd)
                kinjM=0; % injection from slave to master - initially zero
                kinjS=0; % injection from master to slave - initially zero
            else
                kinjM = CavityObj.Inj2to1'/tr.*Ei(mm,k-Nd+1).*(exp(i*2*pi*ActDetVal*(k-Nd+1)*PreferencesObj.SamplingTime))'*exp(i*f_optrad*Tdelay+i*CoupPhs);
                kinjS = CavityObj.Inj1to2'/tr.*Einj_i(mm,k-Nd+1).*(exp(-i*2*pi*ActDetVal*(k-Nd+1)*PreferencesObj.SamplingTime))'*exp(i*f_optrad*Tdelay+i*CoupPhs);
            end
            
            be1 = sqrt(4*NN1(k)*bb/PreferencesObj.SamplingTime)*(randn(1, nmodes)+1i*rand(1, nmodes))+kinjM';
            be2 = sqrt(4*NN2(k)*bb/PreferencesObj.SamplingTime)*(randn(1, nmodes)+1i*rand(1, nmodes))+kinjS';
            
            Einj_i(mm, k+1) =Einj_i(mm, k).*(exp(ae1*PreferencesObj.SamplingTime))'+(be1./ae1.*(exp(ae1*PreferencesObj.SamplingTime)-1))';
            Ei(mm, k+1)     =Ei(mm, k).*(exp(ae2*PreferencesObj.SamplingTime))'    +(be2./ae2.*(exp(ae2*PreferencesObj.SamplingTime)-1))';
            
            
            %%%%%%%%%%%%%%injection locking is implemented in two modes
            NN1(k+1)=NN1(k)*exp(an1*PreferencesObj.SamplingTime)+bn1/an1*(exp(an1*PreferencesObj.SamplingTime)-1);
            NN2(k+1)=NN2(k)*exp(an2*PreferencesObj.SamplingTime)+bn2/an2*(exp(an2*PreferencesObj.SamplingTime)-1);

            if toc > 50
                break
            end
        end
        
        
        P1=abs(Einj_i(1, 1:end).^2);
        P1s=abs(Ei(1, 1:end).^2);
        
        RealVals = find(isnan(P1),1)-1;
        if isempty(RealVals)
            RealVals = numel(P1);
        end
        
        P1=real(Filt.butterworth(P1(1:RealVals), RealVals,RealVals*PreferencesObj.SamplingTime, 20, 5e9, 0));
        P1s=real(Filt.butterworth(P1s(1:RealVals), RealVals, RealVals*PreferencesObj.SamplingTime, 20, 5e9, 0));
        
        End1 = numel(P1);
        End2 = 2^19-2^17;
        Pout(kj)=nanmean(P1(max(1,End1-2^18):end))*factor;
        Pouts(kj)=nanmean(P1s((max(1,End1-2^18):end)))*factor;
        Poutstd(kj)=nanstd(P1((max(1,End1-2^18):end)))*factor;
        Poutsstd(kj)=nanstd(P1s((max(1,End1-2^18):end)))*factor;
        NNout_m(kj)=nanmean(NN1((max(1,End1-2^18):end)));
        NNout_s(kj)=nanmean(NN2((max(1,End1-2^18):end)));
        NNout_mstd(kj)=nanstd(NN1((max(1,End1-2^18):end)));
        NNout_sstd(kj)=nanstd(NN2((max(1,End1-2^18):end)));
        
        
        Ef(:,kj)  = abs(fftshift(fft(Einj_i(2^18+1:end)))); % Optical frequency ############ Create frequency array
        Ef2(:,kj) = abs(fftshift(fft(Ei(2^18+1:end)))); % Optical frequency ############ Create frequency array
        Ef(:,kj) = Ef(:,kj)/max(Ef(:,kj));
        Ef2(:,kj) = Ef2(:,kj)/max(Ef2(:,kj));
    end
    


toc
end
