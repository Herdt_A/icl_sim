%     ErrorBar = 0;
%     close(waitbar);
%     kj=1:len;
%     dfinj_i=det_int+(kj-1)*det_stp;
%     close all
%     figure(1)
%     p1_1 = errorbar(dfinj_i/1e9, Pout,Poutstd*ErrorBar);
%     hold on
%     p1_2 = errorbar(dfinj_i/1e9, Pouts,Poutsstd*ErrorBar);
%     p1_3 = plot(dfinj_i/1e9, (Pout+Pouts)/2);
%     hold off
%     p1_1.LineWidth = 2;
%     p1_2.LineWidth = 2;
%     p1_3.LineWidth =  2;
%     xlabel('detuning [GHz]')
%     ylabel('optical output power [arb. u.]');
%     legend('ICL 1','ICL 2');
%     Fig.NicePlot;
%     
%     % FrequencyValues2 = FrequencyValues2;
%     
%     Samples_Fig = 2000;
%     Y = linspace(min(dfinj_i)-abs(dfinj_i(end)/2),max(dfinj_i)+abs(dfinj_i(end)/2),Samples_Fig)';
%     C = interp2(dfinj_i(1:end)/1e9',FrequencyValues2',log(Ef),dfinj_i(1:end)/1e9',Y);
%     NumYStep = numel(min(dfinj_i)-abs(dfinj_i(end)/2):det_stp:max(dfinj_i)+abs(dfinj_i(end)/2));
%     for ii = 1:length(dfinj_i)
%         C(:,ii) = Util.noncircshift(C(:,ii),round((Samples_Fig/(NumYStep-1))*dfinj_i(ii)/det_stp/2));
%     end
%     
%     
%     
%     figure(2)
%     p2_1 = pcolor(dfinj_i(1:end)/1e9',Y/1e9,C);
%     p2_1.LineStyle = 'none';
%     ylim([min(dfinj_i)/1e9 max(dfinj_i)/1e9])
%     xlabel('detuning [GHz]')
%     ylabel('op. frequency spectrum - $f_0$ [GHz]');
%     c1 = colorbar;
%     c1.Label.String = 'norm. spectral intensity [dB]';
%     c1.Label.Interpreter = 'Latex';
%     c1.FontSize = 14;
%     caxis([-8 0]);
%     colormap('hot');
%     title('optical spectrum ICL 1')
%     Fig.NicePlot;
%     
%     
%     
%     C2 = interp2(dfinj_i/1e9',FrequencyValues2',log(Ef2),dfinj_i/1e9',Y);
%     for ii = 1:length(dfinj_i)
%         C2(:,ii) = Util.noncircshift(C2(:,ii),round((Samples_Fig/(NumYStep-1))*-dfinj_i(ii)/det_stp/2));
%     end
%     figure(3)
%     p2_1 = pcolor(dfinj_i/1e9',Y/1e9,C2);
%     p2_1.LineStyle = 'none';
%     ylim([min(dfinj_i)/1e9 max(dfinj_i)/1e9])
%     xlabel('detuning [GHz]')
%     ylabel('op. frequency spectrum - $f_0$ [GHz]');
%     c1 = colorbar;
%     c1.Label.String = 'norm. spectral intensity [dB]';
%     c1.Label.Interpreter = 'Latex';
%     c1.FontSize = 14;
%     caxis([-8 0]);
%     colormap('hot');
%     title('optical spectrum ICL 2')
%     Fig.NicePlot;
%     
%     
%     figure(4)
%     [mx,ix] = max(Ef);
%     p4_1 = plot(dfinj_i/1e9,(FrequencyValues2(ix)+dfinj_i/2)/1e9);
%     p4_1.LineWidth = 2;
%     hold on
%     [mx,ix] = max(Ef2);
%     p4_2 = plot(dfinj_i/1e9,(FrequencyValues2(ix)-dfinj_i/2)/1e9);
%     p4_2.LineWidth = 2;
%     hold off
%     xlabel('detuning [GHz]')
%     ylabel('operation frequency difference [GHz]');
%     legend('ICL 1','ICL 2');
%     title('Most significant wavelength');
%     Fig.NicePlot;
%     
%     
%     
%     figure(5)
%     p5_1 = errorbar(dfinj_i(NNout_m>1e8)/1e9, NNout_m(NNout_m>1e8),NNout_mstd(NNout_m>1e8)*ErrorBar);
%     hold on
%     p5_2 = errorbar(dfinj_i(NNout_s>1e8)/1e9, NNout_s(NNout_s>1e8),NNout_sstd(NNout_s>1e8)*ErrorBar);
%     hold off
%     xlabel('detuning [GHz]')
%     ylabel('carrier density [arb. u.]');
%     legend('ICL 1','ICL 2');
%     p5_1.LineWidth = 2;
%     p5_2.LineWidth = 2;
%     Fig.NicePlot;
%     
%     
% 
%     Fig.SaveFigure('FullFilePath',['C:\ICL_Sim\Results\Uni_CoupPhs' num2str(ku-1) 'pihalfv2.fig'])
% end
%
%
% Vm=[dfinj_i(2:end)' diff(NNout_m)'];
% Vs=[dfinj_i(2:end)' diff(NNout_s)'];
%
% Pout_M=[dfinj_i' Pout'];
%
% Pout_S=[dfinj_i' Pouts'];
%
% save('VoltM.txt', 'Vm', '-ascii');
% save('VoltS.txt', 'Vs', '-ascii');
%
% save('powerM.txt', 'Pout_M', '-ascii');
% save('powerS.txt', 'Pout_S', '-ascii');