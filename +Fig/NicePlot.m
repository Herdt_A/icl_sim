function NicePlot()
%NICEPLOT Summary of this function goes here
%   Detailed explanation goes here



ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 14;
% set(gca,'FontName','Times New Roman')
% set(findobj(gcf,'type','axes'),'FontName','Times New Roman','FontSize',14);



ax.XLabel.Interpreter = 'Latex';
ax.YLabel.Interpreter = 'Latex';
% set(get(get(gcf,'CurrentAxes'),'XLabel'),'Interpreter','Latex');
% set(get(get(gcf,'CurrentAxes'),'YLabel'),'Interpreter','Latex');
XText = ax.XLabel.String;
YText = ax.YLabel.String;
% XText = get(get(get(gcf,'CurrentAxes'),'XLabel'),'string');
% YText = get(get(get(gcf,'CurrentAxes'),'YLabel'),'string');

if ~iscell(XText)
    XText = cellstr(XText);
end
if ~iscell(YText)
    YText = cellstr(YText);
end

for i = 1:length(XText)
XText{i} = ['\textbf{' XText{i} '}'];
end
for i = 1:length(YText)
YText{i} = ['\textbf{' YText{i} '}'];
end
ax.XLabel.String = XText;
ax.YLabel.String = YText;
% set(get(get(gcf,'CurrentAxes'),'XLabel'),'string',XText);
% set(get(get(gcf,'CurrentAxes'),'YLabel'),'string',YText);


try
    ax.Colorbar.Label.Interpreter = 'Latex';
    CText = ax.Colorbar.Label.String;
    CText = ['\textbf{' CText '}'];
    ax.Colorbar.Label.String = CText;
end    

end

