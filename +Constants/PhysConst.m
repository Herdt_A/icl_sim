classdef PhysConst
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Lightspeed          = 299792458;
        ElementaryCharge    = 1.6e-19;
        Planck              = 6.63e-34;
    end
    
    methods
    end
end

