classdef ICL1


    properties
gain                = 1e4;          % gain
refIndex            = 4;            % refractive inde
numcascades         = 3;            % number of cascades
SatPam              = 2e-7;         % Nonlinear saturation parameter
am                  = 13e2;         % absorbtion from the output mirror
aint                = 6e2;          % internal absorption coefficient
tau_c               = 3e-9;         % Carrier lifetime
N_0                 = 4.3e8;        % Threshold carrier density
LWE                 = 0.5;          % linewidth enhancement factor
bb                  = 1;            % ?
nmodes              = 1;            % number of modes
wavelength          = 3.230e-6;     % wavelength
laserlength         = 0.9e-3;       % Laser length
DBW                 = 20e12;        % total bandwidth of laser gain emission
DFm                 = 140e9;        % mode frequency detuning
current             = 24e-3;        % current

    end
    methods
    end
end