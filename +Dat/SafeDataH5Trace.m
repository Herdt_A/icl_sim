function [Output] = SafeDataH5Trace(DataCH1,DataCH2,filename,varargin)
% [Output] = SafeDataH5Trace(DataCH1,DataCH2,FullFilePath,varargin)
% This function safes Files in HDF5-Dataformat
% Example: Util.SafeDataH5Trace(xArray,yArray,'C:\NiceFiles\SafeCompressed.h5trc'
%
%
% DataCh1: DataArray containing the Data
%
% FullFilePath: speaks for itself
%
% 
%            ...,'CH3',[Data],... Data Storage for 3rd channel
%            ...,'CH4',[Data],... Data Storage for 4th channel
%            ...,'Append'...      If File already exist append Data to this
%                                 file creating measurement n+1, otherwise
%                                 create new file
%            ...,'CH1_Attributes',{Attributes},...  needs List with 2
%                                                   columns containing the
%                                                   attributes of the
%                                                   Dataset
%            ...,'CH2_Attributes',{Attributes},...  see above
%            ...,'CH3_Attributes',{Attributes},...  see above
%            ...,'CH4_Attributes',{Attributes},...  see above
%            ...,'Gen_Attributes',{Attributes},...  see above (for all
%                                                   datasets)
%            ...,'CH1_Name','Name',... Shortdescription for Dataset of CH1
%            ...,'CH2_Name','Name',... see above
%            ...,'CH3_Name','Name',... see above
%            ...,'CH4_Name','Name',... see above


% Default Values
if size(DataCH2,1) ~= size(DataCH1,1)
    error('DataCH1 and DataCH2 do not have equal length')
end

MeasurementNumber = 1;
if isempty(DataCH2)
    DataCH2 = [];
end





DataCH1_Type = [];
DataCH2_Type = [];

DataCH1_Attributes = [];
DataCH2_Attributes = [];

General_Attributes = [];

Append = false;


if ~isempty(varargin)
    
    if ~isempty(varargin)
        for i=1:length(varargin)
            if strcmp(varargin{i},'Append')
                Append = true;
            end
            
            
            
            Idx = [];
            if strcmp(varargin{i},'MeasurementNumber')
                Idx = i;
            end
            
            
            if ~isempty(Idx)
                MeasurementNumber = varargin{Idx+1};
            end
            
            
            
            Idx = [];
            if strcmp(varargin{i},'CH1_Name')
                Idx = i;
            end
            
            if ~isempty(Idx)
                DataCH1_Type = varargin{Idx+1};
            end
            
            
            
            Idx = [];
            if strcmp(varargin{i},'CH1_Attributes')
                Idx = i;
            end
            
            if ~isempty(Idx)
                DataCH1_Attributes = varargin{Idx+1};
            end
            
            
            Idx = [];
            if strcmp(varargin{i},'Gen_Attributes')
                Idx = i;
            end
            
            if ~isempty(Idx)
                General_Attributes = varargin{Idx+1};
            end
            
            
            Idx = [];
            if strcmp(varargin{i},'CH2_Name')
                Idx = i;
            end
            
            if ~isempty(Idx)
                DataCH2_Type = varargin{Idx+1};
            end
            
            
            Idx = [];
            if strcmp(varargin{i},'CH2_Attributes')
                Idx = i;
            end
            
            if ~isempty(Idx)
                DataCH2_Attributes = varargin{Idx+1};
            end
            
        end
    end
end


if ~strcmp(filename(end-5:end),'.h5trc')
    filename = strcat(filename,'.h5trc');
end


if exist(filename,'file') == 2
    
    
    if ~Append
        
        
        i=0;
        FileDoesExist = true;
        while FileDoesExist
            i = i+1;
            FileDoesExist = exist([filename(1:end-6) '_NewFile' num2str(i,'%03.0f') '.h5trc'],'file') == 2;
            
        end
        
        filename = [filename(1:end-6) '_NewFile' num2str(i,'%03.0f') '.h5trc'];
        warning(['File already exists. New filename:' ' ' filename]);
    else
        A = h5info(filename);
        
        for i = 1:length(A.Groups(1).Datasets)
            MeasurementName = A.Groups(1).Datasets(i).Name;
            MeasurementNumbExisting(i) = str2double(MeasurementName(end-3:end));
        end
        
        
        Idx = find(MeasurementNumbExisting == MeasurementNumber,1);
        
        if ~isempty(Idx)
            MeasurementNumber = max(MeasurementNumbExisting)+1;
        end
        
        
        
    end
    
end




% Write creation_date of File


DataCH1 = single(DataCH1);
h5create(filename,['/CH1' '/Measurement' num2str(MeasurementNumber,'%04.0f')],[size(DataCH1,1) size(DataCH1,2)],'DataType','single','ChunkSize',[min([size(DataCH1,1) 1e4]) size(DataCH1,2)],'Deflate',9);
h5writeatt(filename,'/','creation_date',datestr(now));
h5write(filename,['/CH1' '/Measurement' num2str(MeasurementNumber,'%04.0f')],DataCH1(:,1:end));

if ~isempty(DataCH1_Type)
    h5writeatt(filename,'/CH1','Type',DataCH1_Type);
end

if ~isempty(DataCH1_Attributes)
    for iii = 1:size(DataCH1_Attributes{1,1},1)
        h5writeatt(filename,['/CH1' '/Measurement' num2str(MeasurementNumber,'%04.0f')],DataCH1_Attributes{1,1}{iii},DataCH1_Attributes{1,2}{iii});
    end
end

if ~isempty(General_Attributes)
    h5writeatt(filename,'/',General_Attributes{:,1},General_Attributes{:,2});
end


DataCH2 = single(DataCH2);
h5create(filename,[ '/CH2' '/Measurement' num2str(MeasurementNumber,'%04.0f')],[size(DataCH2,1) size(DataCH2,2)],'DataType','single','ChunkSize',[min([size(DataCH2,1) 1e4]) size(DataCH2,2)],'Deflate',9);
h5write(filename,[ '/CH2' '/Measurement' num2str(MeasurementNumber,'%04.0f')],DataCH2(:,1:end));


if ~isempty(DataCH2_Type)
    h5writeatt(filename,'/CH2','Type',DataCH1_Type);
end

if ~isempty(DataCH2_Attributes)
    for iii = 1:size(DataCH2_Attributes{1,1},1)
        h5writeatt(filename,['/CH2' '/Measurement' num2str(MeasurementNumber,'%04.0f')],DataCH2_Attributes{1,1}{iii},DataCH2_Attributes{1,2}{iii});
    end
end



Output = filename;

end

