function creategif(Frames,FullFileName,varargin)

if numel(varargin) ==1
    varargin{1} = DelayTime;
else
    DelayTime = .05;
end
for i = 1:length(Frames)
    % Draw plot for y = x.^n

      % Capture the plot as an image 
      frame = getframe(Frames(i)); 
      im = frame2im(frame); 
      [imind,cm] = rgb2ind(im,256); 
      % Write to the GIF File 
      if i == 1 
          imwrite(imind,cm,FullFileName,'gif', 'Loopcount',inf); 
      else 
          imwrite(imind,cm,FullFileName,'gif','WriteMode','append','DelayTime',DelayTime); 
      end 
end
end