function SaveFigure(varargin)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
[reg, prop]=parseparams(varargin);
nargs=length(reg);



Index = FindIndex2('Figure',prop);

if and(~isempty(Index),length(prop)>=Index)
    figHandles = prop{Index};
else
    figHandles = findobj('Type', 'figure');
end



if length(figHandles)==0
    error('No Figures to Save')
end


Index = FindIndex2('FileType',prop);

if and(~isempty(Index),length(prop)>=Index)
    
    FileType = prop{Index};
    
else
    FileType = 'fig';
end



Index = FindIndex2('Resolution',prop);

if and(~isempty(Index),length(prop)>=Index)
    Resolution = prop{Index};
else
    Resolution = 600;
end


Index = FindIndex2('FullFilePath',prop);

if and(~isempty(Index),length(prop)>=Index)
    FullFigurePath{1} = prop{Index};
else
    
    
    Index2 = FindIndex2('Folder',prop);
    
    if and(~isempty(Index2),length(prop)>=Index2)
        FileFolder = prop{Index2};
        
        
        Index3 = FindIndex2('FileName',prop);
        
        if and(~isempty(Index3),length(prop)>=Index3)
            FileName = prop{Index3};
            FullFigurePath{1} = [FileFolder '\' FileName];
        else
            FileName = uiputfile(['*.' FileType],'Figure File',[FileFolder '\Figure']);
            FullFigurePath{1} = [FileFolder '\' FileName];
        end
        
        
        
        
    else
       
        
        Index3 = FindIndex2('FileName',prop);
        
        if and(~isempty(Index3),length(prop)>=Index3)
            FileName = prop{Index3};
            FileFolder = uigetdir;
            FullFigurePath{1} = [FileFolder '\' FileName '.' FileType];
                else
                    [FileName,FileFolder] = uiputfile(['*.' FileType],'Figure File');
                    FullFigurePath{1} = [FileFolder FileName];
        end

    end
    
end



if length(figHandles)>1
    FTypeIndex = strfind(FullFigurePath{1},['.' FileType]);
    SaveFigurePath =FullFigurePath{1}(1:FTypeIndex-1);
    for i=1:length(figHandles)
        FullFigurePath{i} = [SaveFigurePath num2str(i,'%03.0f') '.' FileType];
    end
end





if or(length(figHandles) < 8,~isempty(FindIndex2('NoParFor',prop)))


for  i=1:length(figHandles)
    disp(['Saving...' ' ' FullFigurePath{i}]);
    switch FileType
        case 'png'
            
            print(figHandles(i),'-dpng',FullFigurePath{i},'-opengl',['-r' num2str(Resolution)]); 
        case 'eps'
            print(figHandles(i),'-depsc',FullFigurePath{i},'-painters')
        case 'fig'
            savefig(figHandles(i),FullFigurePath{i});
        otherwise
            
    end
    disp(['Saving...' ' ' FullFigurePath{i} ' ' '... Done!']);
end

else
    warning('Doesnt support transparency, set flag "NoParFor" for transparency')
    h = Ana.Util.parfor_progressbar(length(figHandles),['Saving' ' ' num2str(length(figHandles)) ' ' 'Figures...']);
parfor  i=1:length(figHandles)

    switch FileType
        case 'png'
            print(figHandles(i),'-dpng',FullFigurePath{i},'-painters',['-r' num2str(Resolution)]); 
        case 'eps'
            print(figHandles(i),'-depsc',FullFigurePath{i},'-painters')
        case 'fig'
            savefig(figHandles(i),FullFigurePath{i});
        otherwise
            
    end
    h.iterate(1)
end
close(h)
delete(h)


if ~isempty(FindIndex2('CloseAll',prop))
    close all
end

end

end











function [Index,varargout] = FindIndex2(Name,prop)

Index = strcmp(prop,Name);

Index = find(Index ==1)+1;
varargout{1} = isempty(Index);
end

