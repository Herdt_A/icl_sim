function [Out3,Type] = LoadDataH5Trace(FileName,varargin)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here



FileInfo = h5info(FileName);
MeasurementsReadOut = [];
NumChannels = 1:1:length(FileInfo.Groups);
NumAttributes = length(FileInfo.Groups(1).Datasets(1).Attributes);
NamAttributes = cell(NumAttributes,2);
LongImport = false;
for i=1:NumAttributes
    NamAttributes{i} = FileInfo.Groups(1).Datasets(1).Attributes(i).Name;
end


% MsgNotAll = false;
for i=1:length(varargin)
    Idx = [];
    if strcmp(varargin{i},'Channels')
        Idx = i;
    end
    
    if ~isempty(Idx)
        NumChannels = varargin{Idx+1};
    end
    
    Idx = [];
    if strcmp(varargin{i},'Measurements')
        Idx = i;
        %         MsgNotAll = true;
    end
    
    if ~isempty(Idx)
        MeasurementsReadOut = varargin{Idx+1};
        
        
        
    end
end



if ~isempty(FileInfo.Groups)
    MaxNumChannels = length(FileInfo.Groups);
    
    
    
    
    
    
    
    for i = NumChannels
        NumMeasurements_perChannel(i) = numel(FileInfo.Groups(i).Datasets);
        try
            Type{i} =  h5readatt(FileName,['/CH' num2str(i)],'Type');
        end
        
        if isempty(MeasurementsReadOut)
            MeasurementsReadOut(i,:) = (1:1:NumMeasurements_perChannel(i));
        end
    end
    h=Ana.Util.parfor_progressbar(length(MeasurementsReadOut)*50,'Importing... Attributes...');
    
    if numel(MeasurementsReadOut) > 10
        LongImport = true;
    end
    
    
    
    for i = NumChannels
        
        for ii = MeasurementsReadOut
            
            for iii = 1:NumAttributes
                Out6{i,ii}{iii,1} = NamAttributes{iii};
                try
                    Out2{i,ii}{iii,1} = h5readatt(FileName,['/CH' num2str(i) '/Measurement' num2str(ii,'%04.0f')],NamAttributes{iii});
                catch ME
                    Out2{i,ii}{iii,1} = nan(1,1);
                end
                
            end
            
            h.iterate(6);
            
            %                            Out2{:}
        end
    end
    
    
    
    
    h.message = 'Importing... Data...';
    for i = NumChannels
        
        if LongImport
            
            
            
            parfor ii = MeasurementsReadOut
                SizeAdd = FileInfo.Groups(i).Datasets(ii).Dataspace.Size(2);
                Out{i,ii}(:,1:SizeAdd) = h5read(FileName,['/CH' num2str(i) '/Measurement' num2str(ii,'%04.0f')]);
                h.iterate(43);
            end
            
        else
            
            for ii = MeasurementsReadOut
                SizeAdd = FileInfo.Groups(i).Datasets(ii).Dataspace.Size(2);
                Out{i,ii}(:,1:SizeAdd) = h5read(FileName,['/CH' num2str(i) '/Measurement' num2str(ii,'%04.0f')]);
                h.iterate(43);
            end
            
        end
    end
    
    h.message = 'Importing... Sorting Data...';
    
    for i = NumChannels
        for ii = 1:length(MeasurementsReadOut)
            Out5{i,ii} = Out{i,MeasurementsReadOut(ii)};
            if exist('Out2','var')
            Out4{i,ii} = Out2{i,MeasurementsReadOut(ii)};
            Out7{i,ii} = Out6{i,MeasurementsReadOut(ii)};
            else
                Out4 = [];
                Out7 = [];
            end
            if LongImport
                h.iterate(1);
            end
        end
    end
    
    Out3.Traces = Out5;
    Out3.Attributes = Out4;
    Out3.AttributeNam = Out7;
    try
        close(h);
    end
end

